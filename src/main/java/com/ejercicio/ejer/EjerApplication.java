package com.ejercicio.ejer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EjerApplication {

    public static void main(String[] args) {
        SpringApplication.run(EjerApplication.class, args);
    }
}
