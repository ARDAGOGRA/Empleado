package com.ejercicio.ejer.visual;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Edgar Gómez on 20/07/2017.
 */
public interface MyRepository extends JpaRepository<Empleado, Long> {
}
