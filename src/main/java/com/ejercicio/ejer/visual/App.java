package com.ejercicio.ejer.visual;

import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Edgar Gómez on 20/07/2017.
 */
@SpringUI
public class App extends UI {
    @Autowired
    MyRepository empleadoRepository;

    @Override

    protected void init(VaadinRequest vaadinRequest) {
        VerticalLayout layout = new VerticalLayout();
        HorizontalLayout hlayout = new HorizontalLayout();
        TextField nombre = new TextField("Nombre");
        TextField apellido = new TextField("Apellido");
        TextField edad = new TextField("Edad");
        TextField direccion = new TextField("Dirección");
        TextField telefono = new TextField("Teléfono");
        TextField sueldo = new TextField("Sueldo");

        Grid<Empleado> grid = new Grid<>();
        grid.addColumn(Empleado::getId).setCaption("Id");
        grid.addColumn(Empleado::getNombre).setCaption("Nombre");
        grid.addColumn(Empleado::getApellido).setCaption("Apellido");
        grid.addColumn(Empleado::getEdad).setCaption("Edad");
        grid.addColumn(Empleado::getDireccion).setCaption("Dirección");
        grid.addColumn(Empleado::getTelefono).setCaption("Teléfono");
        grid.addColumn(Empleado::getSueldo).setCaption("Sueldo");

        Button add = new Button("Adicionar");
        add.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Empleado e = new Empleado();
                e.setNombre(nombre.getValue());
                e.setApellido(apellido.getValue());
                e.setEdad(Integer.parseInt(edad.getValue()));
                e.setDireccion(direccion.getValue());
                e.setTelefono(Integer.parseInt(telefono.getValue()));
                e.setSueldo(Double.parseDouble(sueldo.getValue()));
                empleadoRepository.save(e);
                grid.setItems(empleadoRepository.findAll());

                nombre.clear();
                apellido.clear();
                edad.clear();
                direccion.clear();
                telefono.clear();
                sueldo.clear();

                nombre.getCursorPosition();
            }
        });
        Button eli = new Button("Eliminar");
        eli.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                grid.getSelectedItems();
                System.out.println("usted selecciono " + grid);
            }
        });
        layout.addComponents(nombre, apellido, edad);
        layout.addComponents(direccion, telefono, sueldo);
        hlayout.addComponents(add, eli);
        layout.addComponent(hlayout);
        layout.addComponent(grid);
        setContent(layout);
    }
}
